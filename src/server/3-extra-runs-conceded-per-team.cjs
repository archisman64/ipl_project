//  Extra runs conceded per team in the year 2016

const csvToJson = require('csvtojson');

const matchesFilePath='src/data/matches.csv';
const delivieriesFilePath='src/data/deliveries.csv';
const path = require('path');
const fs = require('fs');
const outputFilePath = path.join(__dirname, '../public/output/3-extra-runs-conceded-per-team.json');


(function sovler() {
    let matchIds;

    csvToJson()
    .fromFile(matchesFilePath)
    .then((result)=>{
        
        const filteredIds = result.reduce((acc, value) => {
            if(value.season && value.season === '2016') {
                acc.push(value.id);
            } else {
                acc = [...acc];
            }
            return acc;
        }, []);
        
        matchIds = [...filteredIds];
        // for(let index = 0; index < result.length; index ++) {
        //     if(result[index].season && result[index].season === '2016') {
        //         matchIds.push(result[index].id);
        //     }
        // }
        
        // console.log('match ids',  matchIds);
    })
    .catch(err => console.log(err));


    csvToJson()
    .fromFile(delivieriesFilePath)
    .then((deliveries)=>{

        const extraRunsPerTeam = deliveries.reduce((acc, value) => {
            // console.log('value:', value);
            const bowlingTeam = value.bowling_team.trim();
            const extraRuns = +value.extra_runs;
            if(matchIds.includes(value.match_id)) {
                if(!acc[bowlingTeam]) {
                    acc[bowlingTeam] = extraRuns;
                } else {
                    acc[bowlingTeam] += extraRuns;
                }
            } else {
                acc = {...acc};
            }
            return acc;
        }, {});

        // for(let index = 0; index < matchIds.length; index ++) {
        //     // console.log(result[index].season);
        //     for(let newIndex = 0; newIndex < deliveries.length; newIndex ++) {
        //         if(deliveries[newIndex] && deliveries[newIndex].match_id === matchIds[index]){
        //             const delivery = deliveries[newIndex];
    
        //             const bowlingTeam = delivery.bowling_team.trim();
        //             // console.log('bowling team', bowlingTeam)
        //             const extraRuns = +delivery.extra_runs;
        //             if(!extraRunsPerTeam[bowlingTeam]) {
        //                 extraRunsPerTeam[bowlingTeam] = extraRuns;
        //             } else {
        //                 extraRunsPerTeam[bowlingTeam] += extraRuns;
        //             }
        //         }
        //     }
        // }
        // console.log('extra runs per team:', extraRunsPerTeam);
        fs.writeFileSync(outputFilePath, JSON.stringify(extraRunsPerTeam, null, 2), 'utf-8');
    })
    .catch(err => console.log(err));
})();

