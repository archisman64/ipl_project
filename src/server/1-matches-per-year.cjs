//  Number of matches played per year for all the years in IPL
const csvToJson = require('csvtojson');
const path = require('path');
const fs = require('fs');

const matchesFile='src/data/matches.csv';
const outputFilePath=path.join(__dirname, '../public/output/1-matches-per-year.json');

csvToJson()
.fromFile(matchesFile)
.then((result)=>{
    const matchesPerYear = result.reduce((acc, value) => { 
        const season = value.season;
        if(acc[season]) {
            acc[season] = acc[season] + 1;
        } else {
            acc[season] = 1;
        }
        return acc;
    }, {});
    
    fs.writeFileSync(outputFilePath, JSON.stringify(matchesPerYear, null, 2), 'utf-8')
})
.catch(err => console.log(err));