//  Find a player who has won the highest number of Player of the Match awards for each season

const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');

const matchesFilePath=path.join(__dirname ,'../data/matches.csv');
// const delivieriesFilePath=path.join(__dirname, '../data/deliveries.csv');
const outputFilePath=path.join(__dirname, '../public/output/6-player-who-has-won-the-highest-number-of-player-of-the-match-awards-for-each-season.json');

csvToJson()
.fromFile(matchesFilePath)
.then((matchData)=>{
    const playerOfTheMatchCount = matchData.reduce((acc, currMatch) => {
        const year = currMatch.season;
        const playerOfTheMatch = currMatch.player_of_match;
        
        if(!acc[year]) {
            acc[year] = {};
            acc[year][playerOfTheMatch] = 1;
        } else {
            if(!acc[year][playerOfTheMatch]) {
                acc[year][playerOfTheMatch] = 1;
            } else {
                acc[year][playerOfTheMatch] ++;
            }
        }
        return acc;
    }, {});

    const result = Object.entries(playerOfTheMatchCount).map(([year, players]) => {
        const sortedPlayers = Object.entries(players).sort((player1, player2) => {
            return player2[1] - player1[1];
        })
        // returning the playing with highest number of awards from the sorted result
        return [year, sortedPlayers[0][0]];
    });
    
    // console.log(Object.fromEntries(result));

	// console.log('result:',  result);
    fs.writeFileSync(outputFilePath, JSON.stringify(Object.fromEntries(result), null, 2), 'utf-8');
})
.catch(err => console.log(err));