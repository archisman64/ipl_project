//  Find the number of times each team won the toss and also won the match
const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');

const matchesFilePath=path.join(__dirname ,'../data/matches.csv');
// const delivieriesFilePath=path.join(__dirname, '../data/deliveries.csv');
const outputFilePath=path.join(__dirname, '../public/output/5-number-of-times-each-team-won-the-toss-and-also-won-the-match.json');

csvToJson()
.fromFile(matchesFilePath)
.then((matchData)=>{
    const result = matchData.reduce((acc, currMatch) => {
        const tossWinner = currMatch.toss_winner.trim();
        // create a new entry if toss winner
        // isn't already present in the result object
        if(!acc[tossWinner]) {
            // initialize the count of winning matches as 0
            acc[tossWinner] = 0;
        }
        const matchWinner = currMatch.winner.trim();
        // increament the winning count if toss winner is the match winner
        if(tossWinner === matchWinner) {
            acc[tossWinner] ++;
        }
        return acc;
    }, {});
    
	// console.log('result:',  result);
    fs.writeFileSync(outputFilePath, JSON.stringify(result, null, 2), 'utf-8')
})
.catch(err => console.log(err));