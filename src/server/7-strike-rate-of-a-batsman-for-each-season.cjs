//  Find the strike rate of a batsman for each season

const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');

const matchesFilePath=path.join(__dirname ,'../data/matches.csv');
const delivieriesFilePath=path.join(__dirname, '../data/deliveries.csv');
const outputFilePath=path.join(__dirname, '../public/output/7-strike-rate-of-a-batsman-for-each-season.json');

(function solver() {
    //  to do mapping of matchId with season
    let matchId_to_Season;
    csvToJson()
    .fromFile(matchesFilePath)
    .then((matchData) => {
        // console.log(matchData)
        matchId_to_Season = matchData.reduce((acc, currMatch) => {
            const year = currMatch.season;
            const matchId = currMatch.id;
            if(!acc[matchId]) {
                acc[matchId] = year;
            }
            return acc;
        }, {});
    })
    .catch((err) => console.log(err));
    
    csvToJson()
    .fromFile(delivieriesFilePath)
    .then((deliveries)=>{
        // console.log(deliveries);

        // to track the runs and number of balls played by each batsman
        const strikeRecord = deliveries.reduce((acc, delivery) => {
            const batsman = delivery.batsman.trim();
            const runs = +delivery.batsman_runs;
            if(batsman.length > 0) {
                const matchId = delivery.match_id;
                const matchSeason = String(matchId_to_Season[matchId]);
    
                if(!acc[batsman]){
                    // creatig empty record of that batsman
                    acc[batsman] = {};
                    acc[batsman][matchSeason] = {};
                    acc[batsman][matchSeason]['runs_scored'] = runs;
                    acc[batsman][matchSeason]['balls_played'] = 1;
                } else {
                    if(!acc[batsman][matchSeason]){
                        // creating season record of already existing batsman
                        acc[batsman][matchSeason] = {};
                        acc[batsman][matchSeason]['runs_scored'] = runs;
                        acc[batsman][matchSeason]['balls_played'] = 1;
                    } else {
                        // adding the runs and balls to last record
                        acc[batsman][matchSeason]['runs_scored'] += runs;
                        acc[batsman][matchSeason]['balls_played'] ++;
                    }
                }
            }
            return acc;
        }, {});

        // stores strike rate (final result)
        const strikeRateRecord = Object.entries(strikeRecord).map(([batsman, info]) => {
            // console.log('batsman:', batsman);
            // console.log('info:', info);
            const infoWithStrikeRate = Object.keys(info).map((seasonKey) => {
                const strikeRate = (info[seasonKey].runs_scored/info[seasonKey].balls_played) * 100;
                return [seasonKey, strikeRate];
            });
            // console.log('strike rate info:', Object.fromEntries(infoWithStrikeRate));
            return [batsman, Object.fromEntries(infoWithStrikeRate)];
        });
        
        // console.log(Object.fromEntries(strikeRateRecord));
        fs.writeFileSync(outputFilePath, JSON.stringify(Object.fromEntries(strikeRateRecord), null, 2), 'utf-8')
    })
    .catch(err => console.log(err));
})();