//  Top 10 economical bowlers in the year 2015
const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');

const matchesFilePath=path.join(__dirname ,'../data/matches.csv');
const delivieriesFilePath=path.join(__dirname, '../data/deliveries.csv');
const outputFilePath=path.join(__dirname, '../public/output/4-top-10-economical-bowlers.json');

(function solver() {

    let matchIds;
    
    csvToJson()
    .fromFile(matchesFilePath)
    .then((result)=>{
    
        const filteredIds = result.reduce((acc, value) => {
            if(value.season && value.season === '2015') {
                acc.push(value.id);
            } else {
                acc = [...acc];
            }
            return acc;
        }, []);
        
        matchIds = [...filteredIds];
    })
    .catch(err => console.log(err));
    
    csvToJson()
    .fromFile(delivieriesFilePath)
    .then((deliveries)=>{
    
        const bowlerRecord = deliveries.reduce((acc, delivery) => {
            if(matchIds.includes(delivery.match_id)) {
                const bowler = delivery.bowler.trim();
                const totalRuns = +delivery.total_runs;
    
                if(!acc[bowler]) {
                    // creating new bowler record when we can't find it
                    acc[bowler] = {};
                    acc[bowler]['runs'] = totalRuns;
                    acc[bowler]['balls'] = 1;
                } else {
                    // incrementing the runs and balls when the record already exists
                    acc[bowler]['runs'] += totalRuns;
                    acc[bowler]['balls'] ++;
                }
            } else {
                acc = {...acc};
            }
            return acc;
        }, {});
    
        const bowlerKeys = Object.keys(bowlerRecord);
    
        const sortedEconomy = bowlerKeys.map((key) => {
            const record = bowlerRecord[key];
            const economy = (record['runs']/record['balls']) * 6;
            return [key, economy];
        }).sort((first, second) => {
            return first[1] - second[1];
        });

       const topTenEconomy = Object.fromEntries(sortedEconomy.slice(0, 10));
        
        // console.log('result:',topTenEconomy);
        // writing the result to output file path
        fs.writeFileSync(outputFilePath, JSON.stringify(topTenEconomy, null, 2), 'utf-8');
    })
    .catch(err => console.log(err));
})();