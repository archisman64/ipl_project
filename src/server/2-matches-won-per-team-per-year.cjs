//  Number of matches won per team per year in IPL
const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesFile='src/data/matches.csv';
const path = require('path');

const outputFilePath=path.join(__dirname, '../public/output/2-matches-won-per-team-per-year.json');

csvToJson()
.fromFile(matchesFile)
.then((result)=>{
    const matchesWonPerTeam = result.reduce((acc, value) => {
        if(value.winner && value.season) {
            const winningTeam = value.winner.trim();
            const year = value.season;
            const team1 = value.team1.trim();
            const team2 = value.team2.trim();
            if(!acc[year]) {
                acc[year] = {};
                acc[year][team1] = 0;
                acc[year][team2] = 0;
                // increamenting winning team's count
                acc[year][winningTeam] ++;
            } else {
                if(!acc[year][team1]) {
                    // creating team1's entry if it doesn't exist
                    acc[year][team1] = 0;
                }
                if(!acc[year][team2]) {
                    // creating team2's entry if it doesn't exist
                    acc[year][team2] = 0;
                }
                // increamenting win count of winning team
                acc[year][winningTeam] ++;
            }
        } else {
            acc = { ...acc };
        }
        return acc;
    }, {});
    
    // iterating through each object
    // for(let index = 0; index <= result.length; index ++) {
    //     if(result[index] && result[index].winner && result[index].season){
    //         // extracting the winnig team's name and year if they exist
    //         const winningTeam = result[index].winner.trim();
    //         const year = result[index].season;
    //         const team1 = result[index].team1.trim();
    //         const team2 = result[index].team2.trim();
    //         // checking if we have a previous record for that year
    //         if(!matchesWonPerTeam[year]) {
    //             // creating new entry for that year
    //             matchesWonPerTeam[year] = {};
    //             // creating new entries for both teams as we didn't have any entry for this year
    //             matchesWonPerTeam[year][team1] = 0;
    //             matchesWonPerTeam[year][team2] = 0;
    //             // increamenting winning team's count
    //             matchesWonPerTeam[year][winningTeam] ++;
    //         } else {
    //             // we already have that year's entry
    //             // just have to check for the team's record
    //             if(!matchesWonPerTeam[year][team1]) {
    //                 // creating team1's entry if it doesn't exist
    //                 matchesWonPerTeam[year][team1] = 0;
    //             }
    //             if(!matchesWonPerTeam[year][team2]) {
    //                 // creating team2's entry if it doesn't exist
    //                 matchesWonPerTeam[year][team2] = 0;
    //             }
    //             // increamenting win count of winning team
    //             matchesWonPerTeam[year][winningTeam] ++;
    //         }
    //     }
    // }
	// console.log('matches won per year', matchesWonPerTeam); 
    fs.writeFileSync(outputFilePath, JSON.stringify(matchesWonPerTeam, null, 2), 'utf-8');
})
.catch(err => console.log(err));